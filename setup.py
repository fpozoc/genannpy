#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(name='genannpy',
      version='1.0.0',
      description='Genome annotation python useful tools.',
      author='Fernando Pozo',
      author_email='fpozoc@cnio.es',
      url='https://gitlab.com/fpozoc',
      download_url='https://gitlab.com/bu_cnio/genannpy/-/archive/master/genannpy-master.tar.gz',
      license='GNU General Public License',
      install_requires=
            ['pandas', 
             'numpy',
             'biopython',
             'gtfparse', 
             ],
      extras_require={
            'interactive': [
                  'jupyterlab',
                  'watermark'
            ],
      },
      packages=find_packages()
     )