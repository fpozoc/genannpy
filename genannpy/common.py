#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" genannpy/common.py

Usage: python -m ...
___
--help | -h Display documentation

Module title

Description 

This file can also be imported as a module and contains the following functions:
    * foo: returns str.

TO DO:  
    *
"""

from __future__ import absolute_import, division, print_function

import gzip, os, warnings

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2020"
__license__ = "GNU General Public License"
__version__ = "1.0.1"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"

warnings.filterwarnings('ignore')


def open_files(filepath:str)->str:
    if filepath.endswith('.gz'):
        open_file = gzip.open(filepath, 'rt')
    else: 
        open_file = open(filepath, 'r')
    return open_file


def create_dir(outdir:str):
    if not os.path.exists(outdir):
        os.makedirs(outdir)    
    return outdir
