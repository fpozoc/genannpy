#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" genannpy/appris.py

Usage: python -m ...
___
--help | -h Display documentation

APPRIS data parser

http://apprisws.bioinfo.cnio.es/pub/current_release/datafiles/ 

This file can also be imported as a module and contains the following functions:
    * load_appris
    * load_appris_principal
    * load_corsair
    * load_firestar
    * load_matador3d
    * load_proteo
    * load_thump


TO DO:  
    *
"""

from __future__ import absolute_import, division, print_function

import re, warnings

import pandas as pd
from gtfparse import read_gtf

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2020"
__license__ = "GNU General Public License"
__version__ = "1.0.1"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"

warnings.filterwarnings('ignore')


def load_appris(path:str)->list:
    headers = {
        'gene_id': 'object',
        'gene_name': 'object',
        'transcript_id': 'object',
        'translation_id': 'object',
        'translation': 'category',
        'flags': 'object',
        'start_stop_codons': 'category',
        'ccdsid': 'object',
        'tsl': 'category',
        'length': 'float64',
        'firestar': 'float64',
        'matador3d': 'float64',
        'corsair': 'float64',
        'spade': 'float64',
        'thump': 'float64',
        'crash': 'object',
        'inertia': 'category',
        'proteo': 'float64',
        'score': 'float64',
        'appris': 'category', 
    }
    df = pd.read_csv(path, sep='\t', names=headers)
    df = df[df['translation'] == 'TRANSLATION']
    df['gene_id'] = df['gene_id'].astype(str)
    # df[['crash_p', 'crash_m']] = df['crash'].str.split(',', expand=True)
    # df['crash_p'] = df['crash_p'].replace('-', df.loc[~df['crash_p'].str.contains('-$')]['crash_p'].median()).astype('float64')
    # df['crash_m'] = df['crash_m'].replace('-', df.loc[~df['crash_m'].str.contains('-$')]['crash_m'].median()).astype('float64')
    for name, dtype in headers.items():
        if (dtype == 'float64') and (df[name].dtype == 'object'):
            df.loc[df[name].str.contains(r'[^0-9]', na=False), name] = 0
        df[name] = df[name].astype(dtype)
    # df = df.drop(['translation', 'inertia'], axis=1).reset_index(drop=True)
    return df


def load_appris_principal(path:str)->list:
    headers = {
        'gene_name': 'object',
        'gene_id': 'object',
        'transcript_id': 'object',
        'ccdsid': 'object',
        'label': 'category'
    }
    df = pd.read_csv(path, sep='\t', names=headers)
    return df


def load_corsair(path:str)->list:
    df = read_gtf(path)
    return df


def load_firestar(path:str)->list:
    df = read_gtf(path)
    df['pep_position'] = df['note'].str.extract(r'^pep_position:(.*?),')
    df['ligands'] = df['note'].str.extract(r'ligands:(.*?)$')
    df = df.drop('note', axis=1)
    return df


def load_matador3d(path:str)->list:
    df = read_gtf(path)
    df['pdb_id'] = df['note'].str.extract(r'^pdb_id:(.*?),')
    df['identity'] = df['note'].str.extract(r'identity:(.*?)$')
    df = df.drop('note', axis=1)
    return df


def load_spade(path:str)->list:
    headers = {
        'seqname': 'object', 
        'source': 'object', 
        'feature': 'object', 
        'start': 'int64', 
        'end': 'int64', 
        'score': 'float64', 
        'strand': 'object',
        'frame': 'int', 
        'transcript_id': 'object', 
        'gene_id': 'object', 
        'hmm_name': 'object', 
        'evalue': 'float64', 
        'pep_start': 'int64',
        'pep_end': 'int64'
            }
    df = read_gtf(path)
    df['hmm_name'] = df['note'].str.extract(r'^hmm_name:(.*?),')
    df['evalue'] = df['note'].str.extract(r'evalue:(.*?),')
    df['pep_start'] = df['note'].str.extract(r'pep_start:(.*?),')
    df['pep_end'] = df['note'].str.extract(r'pep_end:(.*?)$')
    df = df.drop('note', axis=1)
    for col in df.columns:
        df[col] = df[col].astype(headers[col])
    return df


def load_proteo(path:str)->list:
    df = read_gtf(path)
    df['pep_start'] = df['note'].str.extract(r'^pep_start:(.*?),')
    df['pep_end'] = df['note'].str.extract(r'pep_end:(.*?),')
    df['pep_seq'] = df['note'].str.extract(r'pep_seq:(.*?)$')
    df = df.drop('note', axis=1)
    return df


def load_thump(path:str)->list:
    df = read_gtf(path)
    df['pep_start'] = df['note'].str.extract(r'^pep_start:(.*?),')
    df['pep_end'] = df['note'].str.extract(r'pep_end:(.*?)$')
    df = df.drop('note', axis=1)
    return df


# def load_trifid(path:str)->list:
#     return None