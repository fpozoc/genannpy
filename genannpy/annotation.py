#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" genannpy/annotation.py

Usage: python -m ...
___
--help | -h Display documentation

Module title

Description 

This file can also be imported as a module and contains the following functions:
    * foo: returns str.

TO DO:  
    * add loggers
"""

from __future__ import absolute_import, division, print_function

import ftplib, os, re, subprocess, warnings

import pandas as pd
import numpy as np
from .common import open_files

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2020"
__license__ = "GNU General Public License"
__version__ = "1.0.1"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"

warnings.filterwarnings('ignore')

class GTF:
    def __init__(self, path:str=None, db:str=None):
        self.path = path
        self.db = db
    
    @property
    def load(self)->list:
        df = self._read()
        return df

    def get_comments(self):
        ann = '_'.join(self._comments)
        ann_dict = dict()
        if self.db in ['gencode', 'g', 'gn']:
            ann_dict['description'] = re.search(r'##description: (.*?)\n', ann).group(1)
            ann_dict['provider'] = re.search(r'##provider: (.*?)\n', ann).group(1)
            ann_dict['contact'] = re.search(r'##contact: (.*?)\n', ann).group(1)
            ann_dict['format'] = re.search(r'##format: (.*?)\n', ann).group(1)
            ann_dict['date'] = re.search(r'##date: (.*?)\n', ann).group(1)
        elif self.db in ['RefSeq', 'refseq', 'NCBI', 'r', 'rs']:
            ann_dict['gtf_version'] = re.search(r'#gtf-version (.*?)\n', ann).group(1)
            ann_dict['genome-build'] = re.search(r'#!genome-build (.*?)\n', ann).group(1)
            ann_dict['genome-build-accession'] = re.search(r'#!genome-build-accession (.*?)\n', ann).group(1)
            ann_dict['annotation-date'] = re.search(r'#!annotation-date (.*?)\n', ann).group(1)
            ann_dict['annotation-source'] = re.search(r'#!annotation-source (.*?)\n', ann).group(1)
        else:
            pass
        return ann_dict

    def _read(self)->list:
        from gtfparse import read_gtf
        df = read_gtf(self.path)
        if self.db:
            if self.db in ['gencode', 'g', 'gn']:
                df = self._parse_gencode(df)
            elif self.db in ['UniProt', 'uniprot', 'u', 'up']:
                pass # to do
            elif self.db in ['RefSeq', 'refseq', 'NCBI', 'r', 'rs']:
                df = self._parse_refseq(df)
        else:
            pass
        return df

    def _parse_gencode(self, df:list, feature:str='transcript')->list:
        headers = ['seqname', 'source', 'feature', 'start', 'end', 'strand',
                    'gene_id', 'gene_name', 'gene_type', 
                    'transcript_id', 'transcript_name', 'transcript_type', 'protein_id', 'transcript_support_level', 'level', 'ccdsid',
                    'exon_number', 'exon_id', 
                    'tag']
        df = df[headers]
        df['appris'] = df['tag'].str.split('appris_').str[1].str.split(',').str[0]
        # df.loc[df['tag'].str.contains('readthrough_transcript', na=False), 'readthrough'] = 1
        # df['readthrough'] = df['readthrough'].fillna(0)
        df.loc[df['tag'].str.contains('start_NF'), 'NF'] = 'SNF'
        df.loc[df['tag'].str.contains('end_NF'), 'NF'] = 'ENF'
        if feature == 'transcript':
            df = df.loc[df['feature'] == feature].reset_index(drop=True)
            df = df.rename(columns={'transcript_support_level': 'tsl'})
            df['tsl'] = df['tsl'].replace(np.nan, 6)
            df['tsl'] = df['tsl'].replace('', 6)
            df['tsl'] = df['tsl'].replace('NA', 6)
            df['CCDS'] = df['ccdsid'].str.contains('CCDS', na=False).astype(int)
            df['StartEnd_NF'] = df['tag'].str.contains(
                'cds_start_NF|cds_end_NF|mRNA_start_NF|mRNA_end_NF', 
                na=False).astype(int)
            df['RNA_supported'] = df['tag'].str.contains(
                'nested_454_RNA_Seq_supported|RNA_Seq_supported_only|RNA_Seq_supported_partial', 
                na=False).astype(int)
            for feature in ['basic', 'CCDS', 'NAGNAG', 'readthrough']:
                df[feature] = df['tag'].str.contains(feature, 
                na=False).astype(int)
            for feature in ['nonsense_mediated_decay', 'non_stop_decay', 'pseudogene']:
                df[feature] = df['transcript_type'].str.contains(feature, 
                na=False).astype(int)
        return df

    def _parse_refseq(self, df:list, feature:str='transcript')->list:
        headers = ['seqname', 'source', 'feature', 'gbkey', 'start', 'end', 'strand',
                    'hgnc_id', 'gene_id', 'gene_name', 'gene_type', 
                    'transcript_id', 'readthrough', 
                    'exon_number', 
                    'product']
        df = df.rename(columns={'gene':'gene_name', 'gene_biotype':'gene_type'})
        df['gene_id'] = df['tag'].str.extract(r'^GeneID:(.*?)(?:$|,)"')
        df['hgnc_id'] = df['tag'].str.extract(r',HGNC:HGNC:(.*?)(?:$|,)"')
        df.loc[df['product'].str.contains('readthrough', na=False), 'readthrough'] = True
        df = df[headers]
        df = df.loc[df['feature'] == feature]
        return df
    
    def _comments(self)->dict:
        with open_files(self.path) as file:
            comments = []
            for line in file:
                if line.startswith("#"):
                    comments.append(line)         
        return comments

class GFF:
    def __init__(self, path:str, db:str=None):
        self.path = path
        self.db = db
        self.headers = ['seqname', 'source', 'type', 'start', 'end', 'score', 'strand', 'frame', 'tag']

    @property
    def load(self, complete:bool=False)->list:
        df = self._read(complete)
        return df

    def _read(self, extra_attributes:bool=False)->list:
        df = pd.read_csv(self.path, sep='\t', comment='#', compression='gzip', names = self.headers)
        if self.db:
            if self.db in ['gencode', 'g', 'gn']:
                df = self._parse_gencode(df, extra_attributes)
            elif self.db in ['UniProt', 'uniprot', 'u', 'up']:
                pass # to do
            elif self.db in ['RefSeq', 'refseq', 'NCBI', 'r', 'rs']:
                df = self._parse_refseq(df, extra_attributes)
        else:
            pass
        return df

    def _parse_gencode(self, df:list, extra_attributes:bool=False):
        attributes = {
            'main':['ID', 'Parent', 'gene_id', 'transcript_id', 'gene_type', 'gene_name', 
                    'transcript_type','transcript_name', 'exon_id', 'exon_number'],
            'optional':['ont', 'hgnc_id', 'havana_gene', 'havana_transcript', 
                        'transcript_support_level', 'level', 'tag']
                        }
        if extra_attributes:
            attributes = attributes['main'] + attributes['optional']
        else:
            attributes = attributes['main']
        for att in attributes:
            df[att] = df['tag'].str.extract(r'(?:^|;){}=(.*?)(?:$|;)'.format(att))
        return df

    def _parse_refseq(self, df:list, extra_attributes:bool=False):
        attributes = {
            'main': ['ID', 'Parent', 'Dbxref', 'gbkey', 'gene', 'transcript_id', 'gene_biotype', 'protein_id'],
            'optional':['product', 'description', 'exception', 'gene_synonym', 
                        'pseudo', 'model_evidence', 'Name', 'Note', 'inference', 
                        'transl_except', 'function', 'experiment', 'old_locus_tag', 
                        'mobile_element']
                        }
        if extra_attributes:
            attributes = attributes['main'] + attributes['optional']
        else:
            attributes = attributes['main']
        for att in attributes:
            df[att] = df['tag'].str.extract(r'(?:^|;){}=(.*?)(?:$|;)'.format(att))
        df = df.drop('tag', axis=1)
        df['gene_type'] = df['gene_biotype'].fillna(method='ffill')
        df = df.drop('gene_biotype', axis=1)
        df['gene_id'] = df['Dbxref'].str.extract(r'GeneID:(.*?),').fillna(method='ffill')
        df['gene_name'] = df['Parent'].str.split('gene-').str[1].fillna(method='ffill')
        df['transcript_id'] = df['Parent'].str.split('rna-').str[1]
        return df


class Gencode():
    
    def __init__(self, version: int, specie: str):
        self.version = version
        self.specie = specie
        self.url = f'ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_{specie}/release_{version}'
        self.ftpfiles = []
        self.filepath = ''
        
    def connect_ftp(self):    # show last path of file
        url_site = self.url.split('ftp://')[1].split('/')[0]
        url_path = self.url.split(url_site)[1]
        fc = ftplib.FTP(url_site)
        try:
            fc.login()
            print(f'Conexion to {self.url} done.')
        except RuntimeError:
            print('Conexion cannot be established.')
            return
        fc.cwd(url_path)
        self.ftpfiles = fc.nlst()
    
    def show_files(self):
        print(self.ftpfiles)

    def regexes(self) -> []:
        catdict = {
            'all': '.', 
            'transcripts': r'gencode.v\d\d.pc_transcripts.fa.gz',
            'translation': r'gencode.v\d\d.pc_translations.fa.gz',
            'gff3': r'gencode.v\d\d.annotation.gff3.gz',
            'gtf': r'gencode.v\d\d.annotation.gtf.gz',
            'metadata': r'metadata'
            }
        return catdict

    def create_dir(self, dirpath: str) -> str: 
        """mkdir -p python equivalent
        
        Arguments:
            dirpath {str} -- Path to create the new folder
        
        Returns:
            absolute_path {str} -- Absolute path of the new folder
        """    
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        absolute_path = os.path.abspath(dirpath)

    def download(self, outdir: str = '.', type: str = None, pattern: str = None):
        self.connect_ftp()
        self.create_dir(outdir)
        catdict = self.regexes()
        if type == None and pattern == None:
            print(f'Please, select which file you want to download from {self.ftpfiles}')
        filename = [p for p in self.ftpfiles if re.search(catdict[type] if type else pattern, p)]
        urlname = os.path.join(self.url, filename[0])
        try:
            subprocess.call(f'wget -P {outdir} -nc --quiet {urlname}', shell=True)
            filepath = os.path.join(outdir, os.path.basename(urlname))
            print(f'{filepath} downloaded.')
            return filepath
        except subprocess.CalledProcessError:
            print("Failed calling wget - Aborting")
            return None


def load_metadata(filepath:str, headers:list, id_group:str='transcript_id')->list:
    """https://www.gencodegenes.org/human/ Metadata files (ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/_README.TXT)

    Arguments:
        filepath {str} -- path of the metadata file from GENCODE
        headers {list} -- headers of the file

    Keyword Arguments:
        id_group {str} -- group to join the repeated identifiers (default: {'transcript_id'})

    Returns:
        list -- pandas DataFrame of parsed metadata file
    """    
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    if len(set(df[id_group].value_counts().values)) > 1:
        df = df.groupby(id_group).agg(lambda x: tuple(x)).applymap(list).reset_index()
    return df