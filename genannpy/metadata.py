#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" genannpy/metadata.py

Usage: python -m ...
___
--help | -h Display documentation

Module title

Description 

This file can also be imported as a module and contains the following functions:
    * load_annotation_remark
    * load_entrez_gene
    * load_exon_supporting_feature
    * load_gene_source
    * load_hgnc
    * load_gene_source
    * load_pdb
    * load_polya_feature
    * load_pudmed_id
    * load_refseq
    * load_selenocysteine
    * load_swissprot
    * load_transcript_source
    * load_transcript_supporting_feature
    * load_trembl

TO DO:  
    *
"""

from __future__ import absolute_import, division, print_function

import pandas as pd

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2020"
__license__ = "GNU General Public License"
__version__ = "1.0.1"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"




def load_annotation_remark(filepath):
    """Remarks made during the manual annotation of the transcript
        1 - transcript id
        2 - annotation remark

        gencode.vX.metadata.Annotation_remark.gz
    """    
    headers = ['transcript_id', 'annotation_remark']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_entrez_gene(filepath):
    """Entrez gene ids associated to GENCODE transcripts (from Ensembl xref pipeline)
        1 - transcript id
        2 - Entrez Gene id

        gencode.vX.metadata.EntrezGene.gz
    """
    headers = ['transcript_id', 'entrez_id']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_exon_supporting_feature(filepath):
    """Piece of evidence used in the annotation of an exon (usually peptides, mRNAs, ESTs)
        1 - transcript id
        2 - external id of the feature supporting the exon annotation
        3 - external source of the supporting feature
        4 - exon id
        5 - exon coordinates

        gencode.vX.metadata.Exon_supporting_feature.gz
    """
    headers = ['transcript_id', 'uniprot_id', 'exon_source', 'exon_id', 'seqname_pos']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_gene_source(filepath):
    """Source of the gene annotation (Ensembl, Havana, Ensembl-Havana merged model or imported in the case of small RNA and mitochondrial genes)
        1 - transcript id
        2 - gene source

        gencode.vX.metadata.Gene_source.gz
    """
    headers = ['transcript_id', 'gene_source']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_hgnc(filepath):
    """HGNC approved gene symbol (from Ensembl xref pipeline)
        1 - transcript id
        2 - HGNC gene symbol
        3 - HGNC unique id

        gencode.vX.metadata.HGNC.
    """
    headers = ['transcript_id', 'gene_name', 'hgnc']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_pdb(filepath):
    """PDB entries associated to the transcript (from Ensembl xref pipeline)
        1 - transcript id
        2 - PDB id

        gencode.vX.metadata.PDB.gz
    """
    headers = ['transcript_id', 'pdb_id']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_polya_feature(filepath):
    """Manually annotated polyA features overlapping the transcript 3'-end   
        1 - transcript id
        2 - transcript-based start coordinate of the polyA feature
        3 - transcript-based end coordinate of the polyA feature
        4 - transcript chromosome
        5 - transcript start coordinate
        6 - transcript end coordinate
        7 - transcript strand
        8 - polyA feature type ("polyA_site", "polyA_signal", "pseudo_polyA")

        gencode.vX.metadata.PolyA_feature.gz
    """
    headers = ['transcript_id', 'transcript_start', 'transcript_end', 'seqname', 'start', 'end', 'strand', 'tag']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_pudmed_id(filepath):
    """Pubmed ids of publications associated to the transcript (from HGNC website)   
        1 - transcript id
        2 - PubMed id

        gencode.vX.metadata.Pubmed_id.gz
    """
    headers = ['transcript_id', 'pubmed_id']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_refseq(filepath):
    """RefSeq RNA and/or protein associated to the transcript (from Ensembl xref pipeline)
        1 - transcript id
        2 - RefSeq RNA id
        3 - RefSeq protein id (optional)

        gencode.vX.metadata.RefSeq.gz
    """
    headers = ['transcript_id', 'refseq_id']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_selenocysteine(filepath):
    """Amino acid position of a selenocysteine residue in the transcript
        1 - transcript id
        2 - selenocysteine position

        gencode.vX.metadata.Selenocysteine.gz
    """
    headers = ['transcript_id', 'selenoc_position']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_swissprot(filepath):
    """UniProtKB/SwissProt entry associated to the transcript (from Ensembl xref pipeline)    
        1 - transcript id
        2 - UniProtKB/SwissProt accession number
        3 - UniProtKB/SwissProt accession number

        gencode.vX.metadata.SwissProt.gz
    """
    headers = ['transcript_id', 'swissprot_id', 'swissprot_id']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_transcript_source(filepath):
    """Source of the transcript annotation    
        1 - transcript id
        2 - transcript source

        gencode.vX.metadata.Transcript_source.gz
    """
    headers = ['transcript_id', 'transcript_source']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_transcript_supporting_feature(filepath):
    """Piece of evidence used in the annotation of the transcript    
        1 - transcript id
        2 - external id of the feature supporting the transcript annotation
        3 - external source of the supporting feature

        gencode.vX.metadata.Transcript_supporting_feature.gz
    """
    headers = ['transcript_id', 'feature_id', 'feature_source']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df


def load_trembl(filepath):
    """UniProtKB/TrEMBL entry associated to the transcript (from Ensembl xref pipeline)    
        1 - transcript id
        2 - UniProtKB/TrEMBL accession number
        3 - UniProtKB/TrEMBL accession number

        gencode.vX.metadata.TrEMBL.gz
    """
    headers = ['transcript_id', 'trembl_id', 'trembl_id']
    df = pd.read_csv(filepath, sep='\t', compression='gzip', names=headers)
    return df