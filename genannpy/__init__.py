from __future__ import absolute_import, division, print_function

from . import annotation
from . import appris
from . import common
from . import metadata
from . import sequences

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2020"
__license__ = "GNU General Public License"
__version__ = "1.0.1"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"