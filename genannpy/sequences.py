#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" genannpy/sequences.py

Usage: python -m ...
___
--help | -h Display documentation

Module title

Description 

This file can also be imported as a module and contains the following functions:
    * foo: returns str.

TO DO:  
    *
"""

from __future__ import absolute_import, division, print_function

import os, warnings

import pandas as pd
import numpy as np
from Bio import SeqIO

from .common import open_files, create_dir

__author__ = "Fernando Pozo"
__copyright__ = "Copyright 2020"
__license__ = "GNU General Public License"
__version__ = "1.0.1"
__maintainer__ = "Fernando Pozo"
__email__ = "fpozoc@cnio.es"
__status__ = "Development"

warnings.filterwarnings('ignore')


class Fasta:
    """
    Management FASTA sequences class
    
    Usage
    >> f = Fasta(path=fasta_path, db=db_name)
    >> df_fasta = f.load
    >> f.split(outdir='directory', mode='single')
    >> f.split(outdir='directory', mode='batch', nbatch=2000) 
    """    
    def __init__(self, path:str=None, db:str=None):
        self.path = path
        self.db = db

    @property
    def load(self)->list:
        """Loading FASTA sequences file in a pandas DataFrame
        
        Database name has to be inserted in the class declaration. In case of 
        not insert anything, the name of the sequence will be the first word of 
        the identifier.

        Returns:
            list -- pandas DataFrame
        """        
        df = self._read(self._to_df())
        return df

    def split(self, outdir:str, file_id:str=None, mode:str='single', nbatch:int=2000):
        """Splitting the multi FASTA sequence file in several files.

        This function has 2 modes: {'single', 'batch'}. If user wants to split
        their fasta file in batched mode, remember to add the number of
        sequences per batch (nbatch)

        Arguments:
            outdir {str} -- Directory to store the files.

        Keyword Arguments:
            mode {str} -- Mode to split the multi FASTA (default: {'single'})
            nbatch {int} -- Number of sequences per batch. Not used in single
            mode (default: {2000})
        """        
        df = self.load
        create_dir(outdir)
        if mode == 'single':
            for name, fid, fseq in zip(df[file_id], df['id'], df['sequence']):
                name = os.path.join(outdir, f'{name}.fa')
                with open(name, "w") as file:
                    file.writelines(self._output(fid, fseq))
        elif mode == 'batch':
            nsamples = df.shape[0]
            nbatches = int(np.trunc(nsamples/nbatch)+1)
            steps = np.ceil(np.linspace(0, nsamples, num=nbatches+1))
            for i in range(0, nbatches+1):
                if i == nbatches:
                    break
                elif i < nbatches:
                    df_batch = df.iloc[int(steps[i]):int(steps[i+1])]
                    seqs_list = self._seqs_list(df_batch)
                    name = os.path.join(outdir, f'{os.path.basename(self.path)}.{i}')
                    with open(name, 'w') as file:
                        file.writelines(seqs_list)

    def to_fasta(self, df:list, filename:str):
        """From pandas DataFrame to FASTA file

        This function inverts the normal use of this class. 
        Here the user must provide a preloaded pandas DataFrame which contains
        sequences in order to out a new customized FASTA file.

        Arguments:
            df {list} -- preloaded pandas DataFrame with FASTA sequences.
            filename {str} -- name of the file to out
        """        
        with open(filename, 'w') as file:
            file.writelines(self._seqs_list(df)) 

    def _load_seqs_dict(self)->dict:
        seqs = list()
        with open_files(self.path) as fasta_file:
            for record in SeqIO.parse(fasta_file, "fasta"):
                seqs.append({
                    'id': record.description,
                    'sequence': str(record.seq),
                })
        return seqs

    def _to_df(self)->list:
        return pd.DataFrame(self._load_seqs_dict())
    
    def _output(self, fid:str, fseq:str)->str:
        return f">{fid}\n{fseq}\n"
    
    def _read(self, df:str)->list:
        if self.db:
            if self.db in ['gencode', 'g', 'gn']:
                # >protein_id|transcript_id|gene_id|havana_gene|havana_transcript|transcript_name|gene_name|length
                id_order = ['protein_id', 'transcript_id', 'gene_id', 'havana_gene', 'havana_transcript', 'transcript_name', 'gene_name', 'length']
                id_regex = r'|'
            elif self.db in ['UniProt', 'uniprot', 'u', 'up']:
                # >db|UniqueIdentifier|EntryName ProteinName OS=OrganismName OX=OrganismIdentifier [GN=GeneName ]PE=ProteinExistence SV=SequenceVersion
                id_order = ['db', 'UniqueIdentifier', 'EntryName', 'ProteinName', 'OrganismName', 'OrganismIdentifier', 'GeneName', 'ProteinExistence', 'SequenceVersion']
                id_regex = r'\|(.*?)\|(.*?) (.*?) OS=(.*?) OX=(.*?) GN=(.*?) PE=(.*?) SV=(.*?)$'
            elif self.db in ['RefSeq', 'refseq', 'NCBI', 'r', 'rs']:
                # >transcript_id description [specie]
                # id_order = ['protein_id', 'description', 'specie']
                # id_regex = r' (.*?) \[(.*?)\]'
                id_order = ['protein_id', 'transcript_id', 'gene_id', 'gene_name', 'length']
                id_regex = r'|'
            id_split = df['id'].str.split(id_regex)
            for i in range(0, len(id_order)):
                df.insert(i, id_order[i], id_split.str[i])
            # df = df.drop('id', axis=1)
        else:
            pass
        return df


    def _seqs_list(self, df:list, file_id:str='id'):
        return [self._output(fid, seq) for fid, seq in zip(df['id'], df['sequence'])]
                
